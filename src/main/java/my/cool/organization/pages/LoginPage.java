package my.cool.organization.pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;

import com.codeborne.selenide.SelenideElement;

public class LoginPage {

    SelenideElement loginField = $(By.name("user"));
    SelenideElement passwordField = $(By.name("password"));
    SelenideElement resetField = $(By.partialLinkText("Забыли пароль?"));
    SelenideElement backToHome = $(By.partialLinkText("Назад к входу в систему"));
    SelenideElement loginOrEmailField = $(By.name("loginOrEmail"));

    public void doLogin(String username, String password){
        loginField.sendKeys(username);
        passwordField.sendKeys(password);
        passwordField.pressEnter();
    }

    public void getBackToHome(){
        backToHome.click();
    }

    public void resetPassword(String login){
        resetField.click();
        loginOrEmailField.sendKeys(login);
        loginOrEmailField.pressEnter();
    }
}
